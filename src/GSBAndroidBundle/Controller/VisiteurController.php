<?php

namespace GSBAndroidBundle\Controller;

use GSBAndroidBundle\Entity\RapportVisite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class VisiteurController extends Controller
{
  /***
   * Retourne tous les visiteurs
   * @return les visiteurs
   */
  public function getVisiteurAction()
  {
    $em = $this->getDoctrine()->getManager();
    $lesVisiteurs = $em->getRepository("GSBAndroidBundle:Visiteur")->findAll();

    if(empty($lesVisiteurs))
    {
      return new JsonResponse(["message"=>"Liste vide"], Response::HTTP_NOT_FOUND);
    }
    else
    {
      return new JsonResponse($lesVisiteurs);
    }
  }
  /***
   * Retourne les dates de rapport de visite pour $visMatricule sous forme annee-mois
   * @return les dates de rapport de visite
   * @param string $visMatricule Le matricule du visiteur
   */
  public function getLesDatesRVAction($visMatricule)
  {
    $em = $this->getDoctrine()->getManager();
    $lesRV = $em->getRepository("GSBAndroidBundle:RapportVisite")->getCRDateByVisiteur($visMatricule);

    if(empty($lesRV))
    {
      return new JsonResponse(["message"=>"Pas de compte-rendus pour ce visiteur"], Response::HTTP_NOT_FOUND);
    }
    else {
      return new JsonResponse($lesRV);
    }
  }
  /***
   * Retourne les rapport de visites pour un visiteurdonnée et une date
   * annee-mois donnée
   * @return les comptes-rendus
   * @param $visMatricule, $dateMoisAnnee
   *
   */
  public function getLesRVAction($visMatricule, $dateMoisAnnee)
  {
    $em = $this->getDoctrine()->getManager();
    $lesRV = $em->getRepository("GSBAndroidBundle:RapportVisite")->getCRDateByVisiteurAndDate($visMatricule, $dateMoisAnnee);

    if(empty($lesRV))
    {
      return new JsonResponse(["message"=>"Pas de compte-rendus pour ce visiteur"], Response::HTTP_NOT_FOUND);
    }
    else {
      return new JsonResponse($lesRV);
    }
  }

  public function getLeRVAction($visMatricule,$rapNum)
  {
    $em = $this->getDoctrine()->getManager();
    $lesRV = $em->getRepository("GSBAndroidBundle:RapportVisite")->getCRByVisiteurAndNumRap($visMatricule,$rapNum);

    if(empty($lesRV))
    {
      return new JsonResponse(["message"=>"Pas de détails de compte-rendu trouvé"], Response::HTTP_NOT_FOUND);
    }
    else {
      return new JsonResponse($lesRV);
    }
  }
  public function addRVAction(Request $request) {
              $header = "Content-Type: application/json";
              header($header);
              $content = $request->get('RapportVisite');

              if (!empty($content)) {
                  $data = json_decode($content, true);

                  $rapportVisite = new RapportVisite();
                  $em = $this->getDoctrine()->getManager();
                  $leVisiteur = $em->getRepository('GSBAndroidBundle:Visiteur')->findOneBy(array('visMatricule'=> strval($data['vis_mat'])));
                  $lePraticien = $em->getRepository('GSBAndroidBundle:Praticien')->findOneBy(array('praNum'=> strval($data['pra_num'])));
                  $dateRapport = new \DateTime("NOW");
                  $pieces = explode("/", $data['rap_dateVisite']);
                  $dateVisite = new \DateTime();
                  $dateVisite->setDate($pieces[2], $pieces[1], $pieces[0]);
                  $rapportVisite->setPraNum($lePraticien)
                                ->setVisMatricule($leVisiteur)
                                ->setRapDatevisite($dateVisite)
                                ->setRapDaterapport($dateRapport)
                                ->setRapBilan($data['bilan'])
                                ->setRapNum(0);

                  $em->persist($rapportVisite);
                  $em->flush();

              } else {
                  throw new BadRequestHttpException("Contenu vide");
              }

              return new JsonResponse($rapportVisite);
      }
}

<?php

namespace GSBAndroidBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ConnexionController extends Controller
{
    public function connexionAction($login, $mdp)
    {
      $em = $this->getDoctrine()->getManager();

      $leVisiteur = $em->getRepository("GSBAndroidBundle:Visiteur")->findOneBy(array("visLogin"=>$login, "visMdp"=>sha1($mdp)));

      if(empty($leVisiteur))
      {
        return new JsonResponse(array("message"=>"Liste vide"),Response::HTTP_NOT_FOUND);
      }
      else {
        return new JsonResponse($leVisiteur);
      }

    }

}

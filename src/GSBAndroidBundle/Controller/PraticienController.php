<?php

namespace GSBAndroidBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class PraticienController  extends Controller
{
  public function getLesPraticiensAction()
  {
    $em = $this->getDoctrine()->getManager();
    $lesPraticiens = $em->getRepository("GSBAndroidBundle:Praticien")->findBy(array("praVisiteur"=>$visMatricule));

    if(empty($lesPraticiens))
    {
      return new JsonResponse(["message"=>"Liste vide"], Response::HTTP_NOT_FOUND);
    }
    else
    {
      return new JsonResponse($lesPraticiens);
    }
  }

}

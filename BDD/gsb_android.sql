-- MySQL dump 10.13  Distrib 5.5.58, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: GsbCRCachan
-- ------------------------------------------------------
-- Server version	5.5.58-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Praticien`
--

DROP TABLE IF EXISTS `Praticien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Praticien` (
  `pra_num` int(11) AUTO_INCREMENT,
  `pra_nom` varchar(50) DEFAULT NULL,
  `pra_prenom` varchar(60) DEFAULT NULL,
  `pra_adresse` varchar(100) DEFAULT NULL,
  `pra_cp` varchar(10) DEFAULT NULL,
  `pra_ville` varchar(50) DEFAULT NULL,
  `pra_coefNotoriete` float DEFAULT NULL,
  `pra_typeCode` varchar(6) DEFAULT NULL,
  `pra_visiteur` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pra_num`),
  KEY `FK_Praticien_Type_Praticien` (`pra_typeCode`),
  KEY `FK_Praticien_Visiteur` (`pra_visiteur`),
  CONSTRAINT `FK_Praticien_Type_Praticien` FOREIGN KEY (`pra_typeCode`) REFERENCES `Type_Praticien` (`type_code`),
  CONSTRAINT `FK_Praticien_Visiteur` FOREIGN KEY (`pra_visiteur`) REFERENCES `Visiteur` (`vis_matricule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Praticien`
--

LOCK TABLES `Praticien` WRITE;
/*!40000 ALTER TABLE `Praticien` DISABLE KEYS */;
INSERT INTO `Praticien` VALUES
(null,'Notini','Alain','114 r Authie','85000','LA ROCHE SUR YON',290.03,'MH','a131'),
(null,'Gosselin','Albert','13 r Devon','41000','BLOIS',307.49,'MV','a131'),
(null,'Delahaye','André','36 av 6 Juin','25000','BESANCON',185.79,'PS','a131'),
(null,'Leroux','André','47 av Robert Schuman','60000','BEAUVAIS',172.04,'PH','a131'),
(null,'Desmoulins','Anne','31 r St Jean','30000','NIMES',94.75,'PO','a131'),
(null,'Mouel','Anne','27 r Auvergne','80000','AMIENS',45.2,'MH','a131'),
(null,'Desgranges-Lentz','Antoine','1 r Albert de Mun','29000','MORLAIX',20.07,'MV','a17'),
(null,'Marcouiller','Arnaud','31 r St Jean','68000','MULHOUSE',396.52,'PS','a17'),
(null,'Dupuy','Benoit','9 r Demolombe','34000','MONTPELLIER',395.66,'PH','a17'),
(null,'Lerat','Bernard','31 r St Jean','59000','LILLE',257.79,'PO','a17'),
(null,'Marçais-Lefebvre','Bertrand','86Bis r Basse','67000','STRASBOURG',450.96,'MH','a17'),
(null,'Boscher','Bruno','94 r Falaise','10000','TROYES',356.14,'MV','a17'),
(null,'Morel','Catherine','21 r Chateaubriand','75000','PARIS',379.57,'PS','c14'),
(null,'Guivarch','Chantal','4 av Gén Laperrine','45000','ORLEANS',114.56,'PH','c14'),
(null,'Bessin-Grosdoit','Christophe','92 r Falaise','6000','NICE',222.06,'PO','c14'),
(null,'Rossa','Claire','14 av Thiès','6000','NICE',529.78,'MH','c14'),
(null,'Cauchy','Denis','5 av Ste Thérèse','11000','NARBONNE',458.82,'MV','c14'),
(null,'Gaffé','Dominique','9 av 1ère Armée Française','35000','RENNES',213.4,'PS','c14'),
(null,'Guenon','Dominique','98 bd Mar Lyautey','44000','NANTES',175.89,'PH','c14');

/*!40000 ALTER TABLE `Praticien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rapport_Visite`
--

DROP TABLE IF EXISTS `Rapport_Visite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rapport_Visite` (
  `vis_matricule` varchar(20) NOT NULL,
  `rap_num` int(11) NOT NULL,
  `pra_num` int(11),
  `rap_bilan` varchar(510) DEFAULT '',
  `rap_dateVisite` date DEFAULT NULL,
  `rap_dateRapport` date DEFAULT NULL,
  PRIMARY KEY (`vis_matricule`,`rap_num`),
  KEY `FK_RV_Praticien` (`pra_num`),
  CONSTRAINT `FK_RV_Praticien` FOREIGN KEY (`pra_num`) REFERENCES `Praticien` (`pra_num`),
  CONSTRAINT `FK_RV_Visiteur` FOREIGN KEY (`vis_matricule`) REFERENCES `Visiteur` (`vis_matricule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rapport_Visite`
--

LOCK TABLES `Rapport_Visite` WRITE;
/*!40000 ALTER TABLE `Rapport_Visite` DISABLE KEYS */;
INSERT INTO `Rapport_Visite` VALUES
('a17', 1, 1,'Rendez-vous annulé. Praticien appelé pour une urgence. A rappeler pour rendez-vous ultérieur','2019-03-01','2019-03-05'),
('a17', 2, 2,'Très agréable. Intéressée par les produits. Echange intéressant. A demandé des échantillons spécifiques','2019-03-04','2019-03-05'),
('a17', 3, 1,'Entretien expéditif. Praticien non convaincu. Rendez-vous pris dans un mois','2019-03-11' ,'2019-03-15' ),
('a17', 4, 3,'Rendez-vous annulé. Praticien appelé pour une urgence. A rappeler pour rendez-vous ultérieur', '2019-03-13' ,'2019-03-15'),
('a17', 5, 3,'Accueil chaleureux. Praticien cordial. A été convaincu par les produits présentés', '2019-04-01' ,'2019-04-05'),
('a17', 6, 4,'Très agréable. Intéressée par les produits. Echange intéressant. A demandé des échantillons spécifiques','2019-04-03' ,'2019-04-05' ),
('a17', 7, 4,'Accueil chaleureux. Praticien cordial. A été convaincu par les produits présentés', '2019-04-03' ,'2019-04-05' ),
('a17', 8, 1,'Très agréable. Intéressée par les produits. Echange intéressant. A demandé des échantillons spécifiques','2019-04-03' ,'2019-04-05'),
('a131',1,1,'Accueil chaleureux. Praticien cordial. A été convaincu par les produits présentés','2018-02-20','2018-02-20'),
('a131',2,2,'Entretien expéditif. Praticien non convaincu. Rendez-vous pris dans un mois','2018-02-20','2018-02-21'),
('a55',1,3,'Très agréable. Intéressée par les produits. Echange intéressant. A demandé des échantillons spécifiques','2018-03-14','2018-03-14'),
('a55',2,4,'Rendez-vous annulé. Praticien appelé pour une urgence. A rappeler pour rendez-vous ultérieur','2018-02-24','2018-02-24'),
('f4',1,4,'TEST','2018-03-24','2018-03-24');

/*!40000 ALTER TABLE `Rapport_Visite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Type_Praticien`
--

DROP TABLE IF EXISTS `Type_Praticien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Type_Praticien` (
  `type_code` varchar(6) NOT NULL DEFAULT '',
  `type_libelle` varchar(50) DEFAULT NULL,
  `type_lieu` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Type_Praticien`
--

LOCK TABLES `Type_Praticien` WRITE;
/*!40000 ALTER TABLE `Type_Praticien` DISABLE KEYS */;
INSERT INTO `Type_Praticien` VALUES
('MH','Médecin Hospitalier','Hopital ou clinique'),
('MV','Médecine de Ville','Cabinet'),
('PH','Pharmacien Hospitalier','Hopital ou clinique'),
('PO','Pharmacien Officine','Pharmacie'),
('PS','Personnel de santé','Centre paramédical');

/*!40000 ALTER TABLE `Type_Praticien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Visiteur`
--

DROP TABLE IF EXISTS `Visiteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Visiteur` (
  `vis_matricule` varchar(20) NOT NULL DEFAULT '',
  `vis_nom` varchar(50) DEFAULT NULL,
  `vis_prenom` varchar(100) DEFAULT NULL,
  `vis_adresse` varchar(100) DEFAULT NULL,
  `vis_cp` varchar(10) DEFAULT NULL,
  `vis_ville` varchar(60) DEFAULT NULL,
  `vis_login` varchar(25) DEFAULT NULL,
  `vis_mdp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`vis_matricule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Visiteur`
--

LOCK TABLES `Visiteur` WRITE;
/*!40000 ALTER TABLE `Visiteur` DISABLE KEYS */;
INSERT INTO `Visiteur` VALUES
('a131','Villechalane','Louis','8 cours Lafontaine','29000','BREST','lvillachane',sha1('azerty')),
('a17','Andre','David','1 rue Aimon de Chissee','38100','GRENOBLE','dandre',sha1('azerty')),
('a55','Bedos','Christian','1 rue Bénédictins','65000','TARBES','cbedos',sha1('azerty')),
('a93','Tusseau','Louis','22 rue Renou','86000','POITIERS','ltusseau',sha1('azerty')),
('b13','Bentot','Pascal','11 avenue 6 Juin','67000','STRASBOURG','pbentot',sha1('azerty')),
('b16','Bioret','Luc','1 rue Linne','35000','RENNES','lbioret',sha1('azerty')),
('b19','Bunisset','Francis','10 rue Nicolas Chorier','85000','LA ROCHE SUR YON','fbunisset',sha1('azerty')),
('b25','Bunisset','Denise','1 rue Lionne','49100','ANGERS','dbunisset',sha1('azerty')),
('b28','Cacheux','Bernard','114 rue Authie','34000','MONTPELLIER','bcacheux',sha1('azerty')),
('b34','Cadic','Eric','123 rue Caponière','41000','BLOIS','ecadic',sha1('azerty')),
('b4','Charoze','Catherine','100 place Géants','33000','BORDEAUX','ccharoze',sha1('azerty')),
('b50','Clepkens','Christophe','12 rue Fédérico Garcia Lorca','13000','MARSEILLE','cclepkens',sha1('azerty')),
('b59','Cottin','Vincenne','36 square Capucins','5000','GAP','vcottin',sha1('azerty')),
('c14','Daburon','François','13 rue Champs Elysées','88500','MIRECOURT','fdaburon',sha1('azerty')),
('c3','De','Philippe','13 rue Charles Peguy','10000','TROYES','pde',sha1('azerty')),
('c54','Debelle','Michel','181 rue Caponière','88000','EPINAL','mdebelle',sha1('azerty')),
('d13','Debelle','Jeanne','134 rue Stalingrad','44000','NANTES','jdebelle',sha1('azerty')),
('d51','Debroise','Michel','2 avenue 6 Juin','70000','VESOUL','mdebroise',sha1('azerty')),
('e22','Desmarquest','Nathalie','14 rue Fédérico Garcia Lorca','54000','NANCY','ndesmarquest',sha1('azerty')),
('e24','Desnost','Pierre','16 rue Barral de Montferrat','55000','VERDUN','pdesnost',sha1('azerty')),
('e39','Dudouit','Frédéric','18 quai Xavier Jouvin','75000','PARIS','fdudouit',sha1('azerty')),
('e49','Duncombe','Claude','19 avenue Alsace Lorraine','88800','VITTEL','cduncombe',sha1('azerty')),
('e5','Enault-Pascreau','Céline','25B rue Stalingrad','40000','MONT DE MARSAN','cenault',sha1('azerty')),
('e52','Eynde','Valérie','3 rue Henri Moissan','76000','ROUEN','veynde',sha1('azerty')),
('f21','Finck','Jacques','route Montreuil Bellay','74000','ANNECY','jfinck',sha1('azerty')),
('f39','Frémont','Fernande','4 rue Jean Giono','69000','LYON','ffremont',sha1('azerty')),
('f4','Gest','Alain','30 rue Authie','46000','FIGEAC','agest',sha1('azerty'));

/*!40000 ALTER TABLE `Visiteur` ENABLE KEYS */;

UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-13 19:52:33
